package org.wit.stafftrack.console.models

import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

var lastId = 0L

internal fun getId(): Long {
    return lastId++
}

class StaffTrackMemStore : StaffTrackStore {

    val entrys = ArrayList<StaffTrackModel>()

    override fun findAll(): List<StaffTrackModel> {
        return entrys
    }

    override fun findOne(id: Long) : StaffTrackModel? {
        var foundStaffTrack: StaffTrackModel? = entrys.find { p -> p.id == id }
        return foundStaffTrack
    }

    override fun create(staffTrack: StaffTrackModel) {
        staffTrack.id = getId()
        entrys.add(staffTrack)
        logAll()
    }

    override fun update(staffTrack: StaffTrackModel) {
        var foundEntry = findOne(staffTrack.id!!)
        if (foundEntry != null) {
            foundEntry.title = staffTrack.title
            foundEntry.description = staffTrack.description
            foundEntry.date = staffTrack.date

        }
    }

    override fun delete(staffTrack: StaffTrackModel) {
        entrys.remove(staffTrack)
    }

    internal fun logAll() {
        entrys.forEach { logger.info("${it}") }
    }
}