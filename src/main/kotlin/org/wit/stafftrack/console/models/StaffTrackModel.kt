package org.wit.stafftrack.console.models

data class StaffTrackModel(var id: Long? = 0,
                           var title: String = "",
                           var description: String = "",
                            var date: String = "")
