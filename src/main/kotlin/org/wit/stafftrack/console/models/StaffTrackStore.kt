package org.wit.stafftrack.console.models

interface StaffTrackStore {
    fun findAll(): List<StaffTrackModel>
    fun findOne(id: Long): StaffTrackModel?
    fun create(staffTrack: StaffTrackModel)
    fun update(staffTrack: StaffTrackModel)
    fun delete(staffTrack: StaffTrackModel)
}