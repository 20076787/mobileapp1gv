package org.wit.stafftrack.console.models

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import javafx.collections.ObservableList
import mu.KotlinLogging
import org.wit.stafftrack.console.helpers.exists
import org.wit.stafftrack.console.helpers.read
import org.wit.stafftrack.console.helpers.write
import java.util.*

private val logger = KotlinLogging.logger {}

val JSON_FILE = "entrys.json"
val gsonBuilder = GsonBuilder().setPrettyPrinting().create()
val listType = object : TypeToken<java.util.ArrayList<StaffTrackModel>>() {}.type

fun generateRandomId(): Long {
    return Random().nextLong()
}

class StaffTrackJSONStore : StaffTrackStore{

    var entrys = mutableListOf<StaffTrackModel>()

    init {
        if (exists(JSON_FILE)) {
            deserialize()
        }
    }

    override fun findAll(): MutableList<StaffTrackModel> {
        return entrys
    }

    override fun findOne(id: Long) : StaffTrackModel? {
        var foundStaffTrack: StaffTrackModel? = entrys.find { p -> p.id == id }
        return foundStaffTrack
    }

    override fun create(staffTrack: StaffTrackModel) {
        staffTrack.id = generateRandomId()
        entrys.add(staffTrack)
        serialize()
    }

    override fun update(staffTrack: StaffTrackModel) {
        var foundEntry = findOne(staffTrack.id!!)
        if (foundEntry != null) {
            foundEntry.title = staffTrack.title
            foundEntry.description = staffTrack.description
            foundEntry.date = staffTrack.date

        }
        serialize()
    }

    override fun delete(staffTrack: StaffTrackModel) {
        entrys.remove(staffTrack)
        serialize()
    }

    internal fun logAll() {
        entrys.forEach { logger.info("${it}") }
    }

    private fun serialize() {
        val jsonString = gsonBuilder.toJson(entrys, listType)
        write(JSON_FILE, jsonString)
    }

    private fun deserialize() {
        val jsonString = read(JSON_FILE)
        entrys = Gson().fromJson(jsonString, listType)
    }
}