package org.wit.stafftrack.console.main

import org.wit.stafftrack.console.controllers.StaffTrackController


fun main(args: Array<String>) {
    StaffTrackController().start()
}


