package org.wit.stafftrack.console.main

import org.wit.stafftrack.console.views.MenuScreen
import tornadofx.App

class MainApp : App(MenuScreen::class)