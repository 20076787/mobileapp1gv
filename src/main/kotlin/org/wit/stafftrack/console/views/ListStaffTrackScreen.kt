package org.wit.stafftrack.console.views


import org.wit.stafftrack.console.controllers.StaffTrackUIController
import org.wit.stafftrack.console.models.StaffTrackModel
import tornadofx.*

class ListStaffTrackScreen : View("List Entrys") {

    val staffTrackUIController: StaffTrackUIController by inject()
    val tableContent = staffTrackUIController.entrys.findAll()
    var data = tableContent.observable()


    override val root = vbox {
        setPrefSize(600.0, 200.0)
        tableview(data) {
            readonlyColumn("ID", StaffTrackModel::id)
            readonlyColumn("TITLE", StaffTrackModel::title)
            readonlyColumn("DESCRIPTION", StaffTrackModel::description)
            readonlyColumn("DATE", StaffTrackModel::date)

        }
        button("Close") {
            useMaxWidth = true
            action {
                runAsyncWithProgress {
                    staffTrackUIController.closeList()
                }
            }
        }
//        button("Delete"){
//            data = staffTrackUIController.entrys
//
//                buttonbar {
//                    button("DELETE SELECTED") {
//                        action {
//                            val model = tableview(data).selectedItem
//                            when (model) {
//                                null -> return@action
//                                else -> staffTrackUIController.deleteEntry(model)
//                            }
//                        }
//                    }
//                }
//                tableview<CategoryModel> {
//                    tableContent = editModel
//                    items = data
//                }

            }
        }



//fun delete() {
//    entryView.listEntrys(entrys)
//    var searchId = entryView.getId()
//    val entry = search(searchId)
//
//    if(entry != null) {
//        entrys.delete(entry)
//        println("Illness Entry Deleted...")
//        entryView.listEntrys(entrys)
//    }
//    else
//        println("Illness Entry Not Deleted...")
//}