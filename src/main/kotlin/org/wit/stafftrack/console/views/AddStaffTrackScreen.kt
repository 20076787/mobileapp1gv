package org.wit.stafftrack.console.views

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.paint.Color
import org.wit.stafftrack.console.controllers.StaffTrackUIController
import tornadofx.*

class AddStaffTrackScreen : View("Add Entry") {
    val model = ViewModel()
    val _title = model.bind { SimpleStringProperty() }
    val _description = model.bind { SimpleStringProperty() }
    val _date = model.bind { SimpleStringProperty() }
    val staffTrackUIController: StaffTrackUIController by inject()

    override val root = form {
        setPrefSize(600.0, 200.0)
        fieldset(labelPosition = Orientation.VERTICAL) {
            field("Title") {
                textfield(_title).required()
            }
            field("Description") {
                textarea(_description).required()
            }
            field("Date (DDMMYY)") {
                textfield(_date).required()
            }
            button("Add") {
                enableWhen(model.valid)
                isDefaultButton = true
                useMaxWidth = true
                action {
                    runAsyncWithProgress {
                        staffTrackUIController.add(_title.value,_description.value,_date.value)
                    }
                }
            }
            button("Close") {
                useMaxWidth = true
                style {
                    backgroundColor += Color.RED
                }
                action {
                    runAsyncWithProgress {
                        staffTrackUIController.closeAdd()
                    }
                }
            }
        }
    }

    override fun onDock() {
        _title.value = ""
        _description.value = ""
        _date.value = ""
        model.clearDecorators()
    }
}