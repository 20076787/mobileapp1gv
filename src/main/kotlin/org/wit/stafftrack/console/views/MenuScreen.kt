package org.wit.stafftrack.console.views

import com.sun.prism.paint.Paint
import javafx.application.Platform
import javafx.geometry.Orientation
import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import org.wit.stafftrack.console.controllers.StaffTrackUIController
import tornadofx.*

class MenuScreen : View("StaffTrack Main Menu") {

    private val staffTrackUIController: StaffTrackUIController by inject()

    override val root = form {
        setPrefSize(400.0, 200.0)
        fieldset(labelPosition = Orientation.VERTICAL) {
            text("")
            button("Add Illness Entry") {

                isDefaultButton = true
                useMaxWidth = true
                style {
                    fontWeight = FontWeight.EXTRA_BOLD
                    backgroundColor += Color.YELLOW
                    fontFamily = "Comic Sans MS"
                    fontSize = 20.px

                }
                action {
                    runAsyncWithProgress {
                        staffTrackUIController.loadAddScreen()
                    }
                }
            }
            text("")
            button("List Illness Entrys") {

                isDefaultButton = true
                useMaxWidth = true
                style {
                    fontWeight = FontWeight.EXTRA_BOLD
                    backgroundColor += Color.YELLOW
                    fontFamily = "Comic Sans MS"
                    fontSize = 20.px

                }
                action {
                    runAsyncWithProgress {
                        staffTrackUIController.loadListScreen()
                    }
                }
            }
                text("")
                button("Exit") {

                    isDefaultButton = true
                    useMaxWidth = true
                    style {
                        fontWeight = FontWeight.EXTRA_BOLD
                        backgroundColor += Color.RED
                        fontFamily = "Comic Sans MS"
                        fontSize = 20.px
                    }
                    action {
                        runAsyncWithProgress {
                            Platform.exit();
                            System.exit(0);
                        }
                    }

                }
            }
        }
    }