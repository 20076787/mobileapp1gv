package org.wit.stafftrack.console.views

import org.wit.stafftrack.console.models.StaffTrackJSONStore
import org.wit.stafftrack.console.models.StaffTrackModel

class StaffTrackView {

    fun menu() : Int {

        var option : Int
        var input: String?

        println("MAIN MENU")
        println(" 1. Add Illness Entry")
        println(" 2. Update Illness Entry")
        println(" 3. List All Illness Entrys")
        println(" 4. Search Illness Entrys")
        println(" 5. Delete Illness Entry")
        println("-1. Exit")
        println()
        print("Enter Option : ")
        input = readLine()!!
        option = if (input.toIntOrNull() != null && !input.isEmpty())
            input.toInt()
        else
            -9
        return option
    }

    fun listEntrys(entrys: StaffTrackJSONStore) {
        println("List All Illness Entrys")
        println()
        entrys.logAll()
        println()
    }

    fun showEntry(staffTrack : StaffTrackModel) {
        if(staffTrack != null)
            println("Illness Entry Details [ $staffTrack ]")
        else
            println("Entry Not Found...")
    }

    fun addEntryData(staffTrack : StaffTrackModel) : Boolean {

        println()
        print("Enter an Illness Title : ")
        staffTrack.title = readLine()!!
        print("Enter an Illness Description : ")
        staffTrack.description = readLine()!!
        print("Enter your Illness Date (DDMMYY) : ")
        staffTrack.date = readLine()!!

        return staffTrack.title.isNotEmpty() && staffTrack.description.isNotEmpty() && staffTrack.date.isNotEmpty()

    }

    fun updateEntryData(staffTrack : StaffTrackModel) : Boolean {

        var tempTitle: String?
        var tempDescription: String?
        var tempDate: String?


        if (staffTrack != null) {
            print("Enter a new Title for [ " + staffTrack.title + " ] : ")
            tempTitle = readLine()!!
            print("Enter a new Description for [ " + staffTrack.description + " ] : ")
            tempDescription = readLine()!!
            print("Enter a new Date for [ " + staffTrack.date + " ] : ")
            tempDate = readLine()!!

            if (!tempTitle.isNullOrEmpty() && !tempDescription.isNullOrEmpty() && !tempDate.isNullOrEmpty())
            {
                staffTrack.title = tempTitle
                staffTrack.description = tempDescription
                staffTrack.date = tempDate

                return true
            }
        }
        return false
    }

    fun getId() : Long {
        var strId : String? // String to hold user input
        var searchId : Long // Long to hold converted id
        print("Enter id to Search/Update : ")
        strId = readLine()!!
        searchId = if (strId.toLongOrNull() != null && !strId.isEmpty())
            strId.toLong()
        else
            -9
        return searchId
    }
}