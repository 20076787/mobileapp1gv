package org.wit.stafftrack.console.controllers

import mu.KotlinLogging
import org.wit.stafftrack.console.models.StaffTrackJSONStore
import org.wit.stafftrack.console.models.StaffTrackModel
import org.wit.stafftrack.console.views.AddStaffTrackScreen
import org.wit.stafftrack.console.views.ListStaffTrackScreen
import org.wit.stafftrack.console.views.MenuScreen
import tornadofx.*

class StaffTrackUIController : Controller() {

    val entrys = StaffTrackJSONStore()
    val logger = KotlinLogging.logger {}

    init {
        logger.info { "Launching StaffTrack TornadoFX UI App" }
    }
    fun add(_title : String, _description : String, _date : String){

        var entry = StaffTrackModel(title = _title, description = _description, date =_date)
            entrys.create(entry)
            logger.info("Entry Added")
    }

    fun loadListScreen() {
        runLater {
            find(MenuScreen::class).replaceWith(ListStaffTrackScreen::class, sizeToScene = true, centerOnScreen = true)
        }
        entrys.logAll()
    }

    fun loadAddScreen() {
        runLater {
            find(MenuScreen::class).replaceWith(AddStaffTrackScreen::class, sizeToScene = true, centerOnScreen = true)
        }
    }

    fun closeAdd() {
        runLater {
            find(AddStaffTrackScreen::class).replaceWith(MenuScreen::class, sizeToScene = true, centerOnScreen = true)
        }
    }
    fun closeList() {
        runLater {
            find(ListStaffTrackScreen::class).replaceWith(MenuScreen::class, sizeToScene = true, centerOnScreen = true)
        }
    }

}