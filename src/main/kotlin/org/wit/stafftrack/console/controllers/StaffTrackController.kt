package org.wit.stafftrack.console.controllers

import mu.KotlinLogging
import org.wit.stafftrack.console.models.StaffTrackJSONStore
import org.wit.stafftrack.console.models.StaffTrackModel
import org.wit.stafftrack.console.views.StaffTrackView


class StaffTrackController {

    //val entrys = StaffTrackMemStore()
    val entrys = StaffTrackJSONStore()
    val entryView = StaffTrackView()
    val logger = KotlinLogging.logger {}

    init {
        logger.info { "Launching StaffTrack Console App" }
        println("StaffTrack Kotlin App")
    }

    fun start() {
        var input: Int

        do {
            input = menu()
            when (input) {
                1 -> add()
                2 -> update()
                3 -> list()
                4 -> search()
                5 -> delete()
                -99 -> dummyData()
                -1 -> println("Exiting Illness Entry App")
                else -> println("Invalid Option")
            }
            println()
        } while (input != -1)
        logger.info { "Shutting Down Illness Entry Console App" }
    }

    fun menu() :Int { return entryView.menu() }

    fun add(){
        var entry = StaffTrackModel()

        if (entryView.addEntryData(entry))
            entrys.create(entry)
        else
            logger.info("Illness Entry Not Added")
    }

    fun list() {
        entryView.listEntrys(entrys)
    }

    fun update() {

        entryView.listEntrys(entrys)
        var searchId = entryView.getId()
        val entry = search(searchId)

        if(entry != null) {
            if(entryView.updateEntryData(entry)) {
                entrys.update(entry)
                entryView.showEntry(entry)
                logger.info("Illness Entry Updated : [ $entry ]")
            }
            else
                logger.info("Illness Entry Not Updated")
        }
        else
            println("Illness Entry Not Updated...")
    }

    fun search() {
        val entry = search(entryView.getId())!!
        entryView.showEntry(entry)
    }


    fun search(id: Long) : StaffTrackModel? {
        var foundEntry = entrys.findOne(id)
        return foundEntry
    }

    fun delete() {
        entryView.listEntrys(entrys)
        var searchId = entryView.getId()
        val entry = search(searchId)

        if(entry != null) {
            entrys.delete(entry)
            println("Illness Entry Deleted...")
            entryView.listEntrys(entrys)
        }
        else
            println("Illness Entry Not Deleted...")
    }

    fun dummyData() {
        entrys.create(StaffTrackModel(title = "Flu", description = "Influenza & Fever", date = "101020"))
        entrys.create(StaffTrackModel(title= "Hangover", description = "Had too much to drink", date = "270796"))
        entrys.create(StaffTrackModel(title = "Food Poisoning", description = "Ate undercooked chicken", date = "120520"))
    }
}